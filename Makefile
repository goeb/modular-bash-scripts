PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
DOCDIR ?= $(PREFIX)/share/doc
MANDIR ?= $(PREFIX)/share/man

all-docs: htmldocs manpages
htmldocs: $(addsuffix .html, $(basename $(wildcard doc/*.ronn)))
manpages: $(basename $(wildcard doc/*.ronn))

doc/%.html: doc/%.ronn
	ronn --html '$<'

doc/%: doc/%.ronn
	ronn --roff '$<'

clean:
	rm -f doc/mbs.1
	rm -f doc/mbs.1.html

install: all-docs

	install -d '$(DESTDIR)$(BINDIR)'
	install -t '$(DESTDIR)$(BINDIR)'                      -m 755 mbs

	install -d '$(DESTDIR)$(MANDIR)/man1'
	install -t '$(DESTDIR)$(MANDIR)/man1'                 -m 644 doc/mbs.1

	install -d '$(DESTDIR)$(DOCDIR)/modular-bash-scripts'
	install -t '$(DESTDIR)$(DOCDIR)/modular-bash-scripts' -m 644 README
	install -t '$(DESTDIR)$(DOCDIR)/modular-bash-scripts' -m 644 doc/mbs.1.html
	install -t '$(DESTDIR)$(DOCDIR)/modular-bash-scripts' -m 644 doc/mbs.1.ronn

	cp -av example '$(DESTDIR)$(DOCDIR)/modular-bash-scripts'

uninstall:

	rm -f  '$(BINDIR)/mbs'
	rm -f  '$(MANDIR)/man1/mbs.1'
	rm -rf '$(DOCDIR)/modular-bash-scripts/'

.PHONY: all-docs clean htmldocs install manpages uninstall

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=78:
